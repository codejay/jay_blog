@extends('core::website.templates.default')

@section('content')
	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb">
					<li><a href="#"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
					<li class="active">Page</li>
				</ul>
			</div>
		</div>
	</div>
   @if(Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
    @endif
	</section>
	<section id="content">
		<div class="container">
			<h2>{{ ucfirst($page->title) }}</h2> <i>&nbsp Created : {{ $page->created_at->diffForHumans() }}</i><p>
			<p>{{ $page->description }}</p>

			@if($page->slug == "contact")
				@include("pages::website.partials.contact_form")		
			@else
		
			@endif
		</div>
	</section>
@stop