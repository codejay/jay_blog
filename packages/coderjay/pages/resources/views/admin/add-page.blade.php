@extends('core::admin.templates.default')

@section('content')
		<div class="col-sm-9">
			<h2>Add a New Page</h2><br>
			{!! Form::open(array('route' => array('admin.page.store'))) !!}
				<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('title', "Title") !!}
							{!! Form::text('title', "Page Title" ,['class' => "form-control", 'placeholder' => "Page Name"]) !!}
						</div>
					</div>

					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('slug', "Slug") !!}
							{!! Form::text('slug', "Page slug" ,['class' => "form-control"]) !!}
						</div>
					</div>					

					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('status', "Page Status") !!}
							{{ Form::select('status', ["1 "=> "Enabled","0" => "Disabled" ]	, null ,['class' => "form-control"]) }}
						</div>
					</div>

					<div class="col-sm-9">
						<div class="form-group">
							{!! Form::label('excerpt', "Page Excerpt") !!}
							{!! Form::textarea('excerpt', "Page Excerpt" ,[
								'class' => "form-control",
								'rows' => '3',
								'cols' => "8"
								]) !!}
						</div>
					</div>

					<div class="col-sm-9">
						<div class="form-group">
							{!! Form::label('description', "Page Description") !!}
							{!! Form::textarea('description', "Page Body" ,[
								'class' => "form-control",
								'rows' => '5',
								'cols' => "8"
								]) !!}
						</div>
					</div>

					<p>
					<div class="col-sm-10">
						<div class="form-group">
							{!! Form::submit("Add Page" ,['class' => "btn btn-primary"]) !!}
						</div>
					</div>	
					{{ Form::hidden('user_id', 4 ) }}	
				{!! Form::close() !!}			
		</div>

	@include('blog::admin.partials.categories')

		
			
@stop