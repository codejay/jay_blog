@extends('core::admin.templates.default')

@section('content')
		<div class="col-sm-9">
			<h2>Edit Page</h2><br>
			{!! Form::open(array('route' => array('admin.page.update',  $page->id),'method' => 'put')) !!}
				<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('title', "Title") !!}
							{!! Form::text('title',  $page->title  ,['class' => "form-control"	]) !!}
						</div>
					</div>

					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('slug', "Slug") !!}
							{!! Form::text('slug',  $page->slug  ,['class' => "form-control"]) !!}
						</div>
					</div>


					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('status', "Page Status") !!}
							{{ Form::select('status', ["1 "=> "Enabled","0" => "Disabled" ]	, null ,['class' => "form-control"]) }}
						</div>
					</div>

					<div class="col-sm-9">
						<div class="form-group">
							{!! Form::label('excerpt', "Page Excerpt") !!}
							{!! Form::textarea('excerpt',  $page->excerpt  ,[
									'class' => "form-control",
									'rows' => "3",
							]) !!}
						</div>
					</div>

					<div class="col-sm-9">
						<div class="form-group">
							{!! Form::label('description', "Page Description") !!}
							{!! Form::textarea('description',  $page->description  ,[
									'class' => "form-control",
									'rows' => "6",
							]) !!}
						</div>
					</div>

					<div class="col-sm-9">
						<div class="form-group">
							{!! Form::submit("Update page" ,['class' => "btn btn-primary"]) !!}
						</div>
					</div>	
					{{ Form::hidden('user_id', 4 ) }}	
				{!! Form::close() !!}			
		</div>
			
@stop