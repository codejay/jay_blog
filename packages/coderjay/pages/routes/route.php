<?php 

Route::group(["prefix" => "admin"],function(){
	Route::resource('page',"Coderjay\Pages\Http\Controllers\admin\PagesController",[
			"as" => "admin"
		]);
	Route::get("page/{id}/delete",[
		"as" => "admin.page.delete",
		"uses" => "Coderjay\Pages\Http\Controllers\admin\PagesController@deletePage"
	]);

	Route::get("page/{id}/toggle-status",[
		"as" => "admin.page.toggle_status",
		"uses" => "Coderjay\Pages\Http\Controllers\admin\PagesController@togglePageStatus"
	]);

});

Route::group(["prefix" => ""],function(){
	Route::resource('page',"Coderjay\Pages\Http\Controllers\website\PagesController");
});

Route::group(['middleware' => ['web']],function(){
	Route::post('page/contact',[
		"as" => "page.contact",
		"uses" => "Coderjay\Pages\Http\Controllers\website\PagesController@contact"
	]);
});