<?php

namespace Coderjay\Pages\Http\Controllers\website;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller ;

use Coderjay\Pages\Http\Models\Pages; 



class PagesController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $page = Pages::where('slug',$slug)->get()->first();

        return view('pages::website.view')->with("page", $page);
    }

}
