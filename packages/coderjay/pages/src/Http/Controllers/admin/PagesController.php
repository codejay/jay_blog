<?php

namespace Coderjay\Pages\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller ;

use Coderjay\Pages\Http\Models\Pages; 

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Pages::orderBy("id", "desc")->paginate(5);
        
        return view('pages::admin.index')->with("pages", $pages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages::admin.add-page');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->input());
        $page_validator = $this->validate($request, [ 
            'title' => "required",
            'description' => "required",
            'slug' => "required",
        ]);
        // dd("this passed");
        Pages::create($request->input());

        return redirect()->route('admin.page.index')->with("message","You Have Succesfully created a New Pages");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Pages::find($id);

        return view('pages::admin.edit-page')
                ->with('page', $page);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $page = Pages::find($id);
        $page->title = $request->input('title');
        $page->slug = $request->input('slug');
        $page->status = $request->input('status');
        $page->excerpt = $request->input('excerpt');
        $page->description = $request->input('description');
        $page->save();

        return back()->with("message" ,"You Have Succesfully Updated your Page");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deletePage($id)
    {
        Pages::destroy($id);

        return back()->withMessage('You Have succesfully deleted the Page');
    }

    public function togglePageStatus()
    {
        dd("still needa work on dis..");
    }
}
