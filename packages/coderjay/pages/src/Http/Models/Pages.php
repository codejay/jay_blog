<?php 

namespace Coderjay\Pages\Http\Models ; 

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
	protected $fillable = [ "title", "slug", "excerpt", "description", "status"];
}