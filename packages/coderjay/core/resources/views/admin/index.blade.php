@extends('core::admin.templates.default')

@section('content')
	  <div id="wrapper">
        <!-- Sidebar -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>Hello {{ \Auth::user()->name }}</h1>
                        <p>Welcome on the Board Admin!!!!</p>

                        {{-- <p>Make sure to keep all page content within the <code>#page-content-wrapper</code>.</p> --}}
                        <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
@stop
