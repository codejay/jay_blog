<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Jay Blog Admin</title>

      <script data-require="jquery@*" data-semver="2.0.3" src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
  <script data-require="bootstrap@*" data-semver="3.1.1" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
  <link data-require="bootstrap-css@3.1.1" data-semver="3.1.1" rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />

    <!-- Bootstrap Core CSS -->
   {!! Html::style('css/bootstrap.css') !!}

    <!-- Custom CSS -->
    {!! Html::style('css/simple-sidebar.css') !!}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
     <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">

            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#">
                        Jay Blog
                    </a>
                </li>
                <li>
                    <a href="{{ route("admin.home") }}">Dashboard</a>
                </li>
                <li>
                    <a href="{{ route('admin.blog.index') }}">Blogs</a>
                </li>
                <li>
                    <a href="#">Overview</a>
                </li>
                <li>
                    <a href="{{ route('admin.page.index') }}">Pages</a>
                </li>
                 <li>
                    <a href="{{ route('admin.page.index') }}">Users</a>
                </li>
                <li>
                    <a href="#">About</a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <nav class="navbar navbar-inverse">
                    <div class="container-fluid">
                        <div class="pull-right">
                             <div class="navbar-header">
                                <a class="navbar-brand" href="{{ route('home') }}">Jay Blog</a>
                            </div>
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="#">Home</a></li>
                               
                                <li><a href="{{ route('user.logout') }}">Log Out</a></li> 
                            </ul>
                        </div>                       
                    </div>

                </nav>
         @if(Session::has('message'))
            <div class="alert alert-success">
                {{ Session::get('message') }}
            </div>
         @endif
                   @yield('content')
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
  
    <!-- /#wrapper -->

    <!-- jQuery -->
   {!! Html::script('js/jquery.js') !!}


    <!-- Bootstrap Core JavaScript -->
   {!! Html::script('js/bootstrap.min.js') !!}

    <!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>

</body>

</html>
