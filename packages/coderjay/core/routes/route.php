<?php 
	

// Route::get('/register',function(){
// 	return "test";
// });

Route::get('/',[
		"as" => "home",
		"uses" => "Coderjay\Core\HomeController@index"
	])->middleware('web');

Route::group(['middleware' => ['web']], function () {

	Route::get('register',[
		'as' => "user.register",
		"uses"=> "Coderjay\Core\Http\Controllers\UserController@register"
	]);

	Route::post("register",[
		"as" => "user.register",
		"uses" => "Coderjay\Core\Http\Controllers\UserController@postRegister"
	]);

	Route::get('login',[
		"as" => "user.login",
		"uses" => "Coderjay\Core\Http\Controllers\UserController@getLogin"
	]);

	Route::post("login",[
		"as" => "user.login",
		"uses" => "Coderjay\Core\Http\Controllers\UserController@postLogin"
	]);

	Route::get('logout',[
		"as" => "user.logout",
		"uses" => "Coderjay\Core\Http\Controllers\UserController@Logout"
	]);

	

});

Route::get('admin/home',[
		"as" => "admin.home",
		"uses" => "Coderjay\Core\Http\Controllers\UserController@adminHome"
	])->middleware('web');

