<?php 
namespace Coderjay\Core\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Media extends Authenticatable
{
    // use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'filename', 'status' , "mediable_id", "mediable_type"
    ];

    public function mediable()
    {
    	return $this->morphTo();
    }

}
