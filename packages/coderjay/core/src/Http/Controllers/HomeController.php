<?php

namespace Coderjay\Core;

use App\Http\Controllers\Controller ; 

class HomeController extends Controller
{
	public function index()
	{
		// echo \Auth::user()->name;
		return view('core::website.index');
	}
} 