<?php 

namespace Coderjay\Core\Http\Controllers ; 

use App\Http\Controllers\Controller ; 
use Illuminate\Http\Request ; 
use Validator;
use Coderjay\Core\Http\Models\User;

class UserController extends Controller
{
	public function register()
	{
		return view('auth.register');
	}

	public function postRegister(Request $request)
	{
		$register_validator = Validator::make($request->all(), [
				"name"=> "required|unique:users",
				"email"=> "required|unique:users",
				"password" => "required|min:3|confirmed"

		]);

		if($register_validator->fails())
		{
			return \Redirect::back()
					->withErrors($register_validator);
		}

		User::create([
			'name' => $request->input('name'),
			'email' => $request->input('email'),
			'password' => bcrypt($request->input('name')),
			'role' => 2
		]);

		return \Redirect::back()->withMessage("You Have succesfully created your Account");
	}

	public function getlogin()
	{
		echo \Auth::check();

		return view("auth.login");
	}

	public function postLogin(Request $request)
	{
		$login_validator = Validator::make($request->all(), [
				"email"=> "required",
				"password" => "required|min:3"

		]);

		if($login_validator->fails())
		{
			return \Redirect::back()
					->withErrors($login_validator);
		}
		if( \Auth::attempt(["email"=> $request->input('email'), "password"=> $request->input('password')]))
		{
			if(\Auth::user()->role == 1)
			{
				return redirect()->route('admin.home');
			}
			else
			{
				return redirect()->route('home')->withMessage("You Have Logged In");
			}
		}
		else 
			return \Redirect::back()
					->withMessage("Incorrect Credentials");
	}

	public function logout()
	{
		\Auth::logout();
		return redirect()->route('user.login')->withMessage('You Have Succesfully Logged Out');
	}

	public function adminHome()
	{
		return view('core::admin.index')->with("user",\Auth::user());
	}

	public function contact()
	{
		dd("contact");
	}



}