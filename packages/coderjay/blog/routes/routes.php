<?php 

//resoureful routing for blog

Route::group(['prefix' => "","middleware" => "web"],function(){
	Route::resource('blog','Coderjay\Blog\Http\Controllers\website\BlogController');

	Route::post('blog/search',[
		"as" => "blog.search",
		"uses" => "Coderjay\Blog\Http\Controllers\website\BlogController@blogSearch"
	]);

	Route::get('category/{id}/blogs',[
		"as" => "category.posts",
		"uses" => "Coderjay\Blog\Http\Controllers\website\BlogController@getBlogsByCategory"
	]);

	Route::post('/blog/{id}/comment/',[
	"as" => "blog.post.comment",
	"uses" => "Coderjay\Blog\Http\Controllers\website\BlogController@postComment"
]);
});


//Blogs Admin
Route::group(['prefix' => "admin","middleware" => "web"],function(){
	Route::resource('blog','Coderjay\Blog\Http\Controllers\Admin\BlogController',
		["as" => "admin"]);

	Route::get('blog/{}/delete',[
		"as" => "admin.blog.delete"
	]);

	Route::get("blog/{id}/delete",[
		"as" => "admin.blog.delete",
		"uses" => "Coderjay\Blog\Http\Controllers\Admin\BlogController@deleteBlogPost"
	]);

	Route::get("blog/{id}/toggle/status",[
		"as" => "admin.blog.toggle_status", 
		"uses" => "Coderjay\Blog\Http\Controllers\Admin\BlogController@toggleStatus"
	]);

	Route::get("blog/{id}/media/delete",[
		"as" => "admin.blog.delete.media", 
		"uses" => "Coderjay\Blog\Http\Controllers\Admin\BlogController@deleteMedia"
	]);
});

