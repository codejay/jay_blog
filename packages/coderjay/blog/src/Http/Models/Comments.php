<?php 

namespace Coderjay\Blog\Http\Models ;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
	protected $table = "blog_comments";

	protected $fillable = ["blog_id", "user_id", "comment"];
}