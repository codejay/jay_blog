<?php

namespace Coderjay\Blog\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = "blog_category";

    protected $fillable = ['title', 'slug'];

    public function blogs()
    {
    	return $this->hasMany('Coderjay\Blog\Http\Models\Blogs', "category_id");
    }
}	
