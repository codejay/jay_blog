<?php

namespace Coderjay\Blog\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Coderjay\Core\Models\Media ; 

class Blogs extends Model
{
    protected $table = "blogs";

    protected $fillable = ['title', 'slug', 'body','category_id','status','media_id'];

    public function category()
    {
    	return $this->belongsTo('Coderjay\Blog\Http\Models\Categories', "category_id");
    }

    public function comments()
    {
    	return $this->hasMany("Coderjay\Blog\Http\Models\Comments", "blog_id");
    }

    public function media()
    {
        return $this->morphMany("Coderjay\Core\Http\Models\Media", "mediable");
    }
}
