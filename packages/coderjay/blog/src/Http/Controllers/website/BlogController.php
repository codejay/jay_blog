<?php

namespace Coderjay\Blog\Http\Controllers\website;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;
use Coderjay\Blog\Http\Models\Blogs;
use Coderjay\Blog\Http\Models\Categories;
use Coderjay\Blog\Http\Models\Comments ; 


class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blogs::orderBy("id" , "desc")->paginate(3);
        return view("blog::website.index")->with("blogs", $blogs);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog = Blogs::find($id);
        $latest_blogs = Blogs::orderBy('id',"desc")->limit(3)->get();

        return view('blog::website.view')
            ->with('blog', $blog)
            ->with('latest_blogs', $latest_blogs);
    }

    public function postComment($blog_id, Request $request)
    {
        Comments::create([
                'blog_id' => $blog_id,
                'user_id' => $request->input('user_id'), 
                'comment' => $request->input('comment')
            ]);

        return back()->with('message', "You Have succesfully posted your opinion");
    }

    public function getBlogsByCategory($cat_id)
    {
        $current_category = Categories::where("id", $cat_id)->first();
        $blogs_by_category = Blogs::where('category_id',$cat_id)->paginate(2);
        $latest_blogs = Blogs::orderBy('id',"desc")->limit(3)->get();

        return view('blog::website.index')
            ->with('blogs', $blogs_by_category)
            ->with('current_category', $current_category->title);
    }

    public function blogSearch(Request $request)
    {
        $search_result = Blogs::where('title',"LIKE","%".$request->input('search_query')."%")->paginate(4);
        $latest_blogs = Blogs::orderBy('id',"desc")->limit(3)->get();
        
        return view('blog::website.index')
            ->with('search_query', $request->input('search_query'))
            ->with('blogs', $search_result);
    }

}
