<?php

namespace Coderjay\Blog\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;
use Coderjay\Blog\Http\Models\Blogs;
use Coderjay\Blog\Http\Models\Categories;
use Coderjay\Core\Http\Models\Media;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blogs::orderBy('id', "desc")->paginate(5);

        return view('blog::admin.index')
            ->with('blogs',$blogs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(\Auth::check())
        {
            $categories = Categories::all();
            foreach($categories as $category)
            {
                $all_categories[$category->id] = $category->title;
            }

            return view('blog::admin.add-blog')->with("all_categories", $all_categories);
        }
        else
            return redirect()->route('user.login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $old_mask = umask(0);
        $media_id = \DB::table('media')->max('id') + 1;
        $blog_media = new Media;
        mkdir(storage_path("app/media/").$media_id);
        $path = $request->blog_image->storeAs("media/".$media_id,$request->file("blog_image")->getClientOriginalName ());

        umask($old_mask);

       $blog_validator = $this->validate($request, [ 
            'title' => "required",
            'body' => "required",
            'slug' => "required",
            'category_id' => "required"
        ]);

       // dd($media_id, $request->input('status'));
       $blog = new Blogs;

       $blog->create([
            "title" => $request->input('title'),
            "body" => $request->input('body'),
            "slug" => $request->input("slug"), 
            "status" => $request->input("status"), 
            'category_id' => $request->input("category_id"), 
            // "media_id" => $media_id
        ]);

         $blog_media->create([
                "filename" => $request->file("blog_image")->getClientOriginalName (), 
                "status" => 1,
                "mediable_id" => \DB::table('blogs')->max('id') ,
                "mediable_type" => "Coderjay\Blog\Http\Models\Blogs"
            ]);

          // \DB::table('mediable')->insert([
          //       "itemable_id" => \DB::table('blogs')->max('id'),
          //       "itemable_type" => "Coderjay\Blog\Http\Models\Blogs",
          //   ]);

        return redirect()->route('admin.blog.index')->with("message","You Have Succesfully created a New Blog");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blogs::find($id);

        return view('blog::admin.edit-blog')
                ->with('blog', $blog);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $blog = Blogs::find($id);
        if($request->file("blog_image"))
        {
            $old_mask = umask(0);
            $media_id = \DB::table('media')->max('id') + 1;
            $blog_media = new Media;
            mkdir(storage_path("app/media/").$media_id);
            $path = $request->blog_image->storeAs("media/".$media_id,$request->file("blog_image")->getClientOriginalName ());
            umask($old_mask);
        }

        $blog_media = new Media;

        if($request->file("blog_image")){
             $blog_media->create([
                "filename" => $request->file("blog_image")->getClientOriginalName (), 
                "status" => 1,
                "mediable_id" => $id ,
                "mediable_type" => "Coderjay\Blog\Http\Models\Blogs"
            ]);
        }
         
        $blog->title = $request->input('title');
        $blog->body = $request->input('body');
        $blog->slug = $request->input('slug');
        $blog->save();

        return back()->with('message', "You Have Succesfully Updated your Blog");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteBlogPost($id)
    {
        Blogs::destroy($id);

        return back()->withMessage('You Have succesfully deleted the Blog');
    }

    public function toggleStatus()
    {
        dd("toggle status");
    }

    public function deleteMedia()
    {   
        dd("delete image");
    }
}
