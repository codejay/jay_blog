@extends('core::website.templates.default')

@section('content')
	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb">
					<li><a href="#"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
					<li class="active">Blog</li>
				</ul>
			</div>
		</div>
	</div>
	</section>
	<section id="content">
	<div class="container">

	 <i>{{ (isset($search_query)) ? $blogs->count()." Result/s Found for search query '".$search_query."'" : "" }}  </i>
	<h2> {{ (isset($current_category)) ? ucfirst($current_category) : "" }} </h2>
		<div class="row">
			<div class="col-lg-8">
			@foreach($blogs as $blog)
				<article>
						<div class="post-image">
							<div class="post-heading">
								<h3><a href="{{ route("blog.show", $blog->id) }}">{{ ucfirst($blog->title) }}</a></h3>
							</div>
							@if($blog->media->count())
								@foreach($blog->media as $media)
		                            {{ Html::image('../storage/app/media/'.$media->id.'/'.$media->filename , 
		                            		$media->filename , [
		                            			"class" => "img-rounded",
		                            			"width" => 656,
		                            			"height" => 586
		                            		]) }}
								@endforeach
							@else
								{{ Html::image('img/dummies/blog/img1.jpg') }}
							@endif
						</div>
						<p>
							 {{ str_limit($blog->body,150) }}
						</p>
						<div class="bottom-article">
							<ul class="meta-post">
								<li><i class="icon-calendar"></i><a href="#"> {{ $blog->created_at->diffForHumans() }}</a></li>
								<li><i class="icon-user"></i><a href="#"> Admin</a></li>
								<li><i class="icon-folder-open"></i><a href="#"> Blog</a></li>
								<li><i class="icon-comments"></i><a href="#">{{ $blog->comments->count() }} Comments</a></li>
							</ul>
							<a href="{{ route("blog.show", $blog->id) }}" class="pull-right">Continue reading <i class="icon-angle-right"></i></a>
						</div>
					</article>
				@endforeach
				<div id="pagination">
					{{ $blogs->appends(Request::except('page'))->links() }} 
				</div>
			</div>
			<div class="col-lg-4">
					<aside class="right-sidebar">
						@include('blog::website.partials.search')
						@include('blog::website.partials.categories')
						@include('blog::website.partials.latest_blog')
						@include('blog::website.partials.tags')
				</aside>
			</div>
		</div>
	</div>
	</section>
@stop