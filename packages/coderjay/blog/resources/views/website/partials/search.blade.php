<div class="widget">
	{{  Form::open(array('route' => array("blog.search"),"method" => "post")) }}
	<form class="form-search">
		{{ Form::text('search_query',"",["class" => "form-control"]) }}<br>
		{{ Form::submit("Search",['class' => "btn btn-primary"]) }}
	{{ Form::close() }}
</div>