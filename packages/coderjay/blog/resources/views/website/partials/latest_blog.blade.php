<?php 
	$latest_blogs =  Coderjay\Blog\Http\Models\Blogs::limit(3)->get() ;
?>
<div class="widget">
	<h5 class="widgetheading">Latest posts</h5>
	<ul class="recent">
		@foreach($latest_blogs as $latest_blog)
			<li>
				{{ Html::image('img/dummies/blog/65x65/thumb1.jpg', "" , ['class' => "pull-left"]) }}
				<h6>
					<a href="{{ route("blog.show", $latest_blog->id) }}">{{ $latest_blog->title }}</a>
				</h6>
				<p>
					 {{ str_limit($latest_blog->body,30) }}
				</p>
			</li>
		@endforeach
</ul>
</div>

