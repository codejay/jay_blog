<?php 
	$categories =  Coderjay\Blog\Http\Models\Categories::all();
?>
<div class="widget">
	<h5 class="widgetheading">Categories</h5>
	<ul class="cat">
		@foreach($categories as $category)
		<li>
			<i class="icon-angle-right"></i>
			<a href="{{ route("category.posts",$category->id) }}">
				{{ ucfirst($category->title) }}
			</a>
			<span>
				({{ $category->blogs->count() }})
			</span>
		</li>
		@endforeach
	</ul>
</div>