@extends('core::website.templates.default')

@section('content')
	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb">
					<li><a href="#"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
					<li class="active">Blog</li>
				</ul>
			</div>
		</div>
	</div>
   @if(Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
    @endif
	</section>
	<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-8">	
				<article>
						<div class="post-image">
							<div class="post-heading">
								<h3><a href="{{ route("blog.show", $blog->id) }}">{{ ucfirst($blog->title) }}</a></h3>
							</div>
							@if($blog->media->count())
								@foreach($blog->media as $media)
		                            {{ Html::image('../storage/app/media/'.$media->id.'/'.$media->filename , 
		                            		$media->filename , [
		                            			"class" => "img-rounded",
		                            			"width" => 656,
		                            			"height" => 586
		                            		]) }}
								@endforeach
							@else
								{{ Html::image('img/dummies/blog/img1.jpg') }}
							@endif
						</div>
						<p>
								{{ $blog->body }}
						</p>

						<div class="bottom-article">
							<ul class="meta-post">
								<li><i class="icon-calendar"></i><a href="#"> {{ $blog->created_at->diffForHumans() }}</a></li>
								<li><i class="icon-user"></i><a href="#"> Admin</a></li>
								<li><i class="icon-folder-open"></i><a href="#"> Blog</a></li>
								<li><i class="icon-comments"></i><a href="#">{{ $blog->comments->count() }} Comment/s</a></li>
							</ul>
						</div>
						@if($blog->comments->count() < 1)
							<h4>Would you like to have the first say?????</h4>
						@else
							<h4>Comments</h4>
						@endif
							@foreach($blog->comments as $comment)
								<p>{{ $comment->created_at->diffForHumans() }} By 
								<i><b>
									{{ ucfirst(App\User::find($comment->user_id)->name) }}
								</i></b>
								<p>{{ $comment->comment }}</p>
								<hr>
							@endforeach
					</article>
				
					@if(\Auth::check())
						{{  Form::open(array('route' => array("blog.post.comment",$blog->id))) }}
							<div class="form-group">
								{{ Form::textarea('comment',"",[
									'class' => "form-control",
									'rows' => '5',
									'placeholder' => "You know what????"
								]) }}
								<br>
								{{ Form::hidden('user_id', 4 ) }}
								{{ Form::submit('Comment',['class' => "btn btn-primary" ]) }}
							</div>						
						{{ Form::close() }}						
					@else
						Please <a href = "{{ route('user.login') }}"> Login </a> To Comment...
					@endif
			</div>
			<div class="col-lg-4">
				<aside class="right-sidebar">
					@include('blog::website.partials.search')
					@include('blog::website.partials.categories')
					@include('blog::website.partials.latest_blog')
					@include('blog::website.partials.tags')
				</aside>
			</div>
		</div>
	</div>
	</section>
@stop