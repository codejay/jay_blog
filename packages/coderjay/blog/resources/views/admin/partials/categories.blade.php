<?php 
	$categories =  Coderjay\Blog\Http\Models\Categories::all();
 ?>
<div class="col-sm-3">
	<legend>
		<h2>Categories</h2>
	</legend>
	<ul>
		@foreach($categories as $category)
			<h5><li>{{ ucfirst($category->title) }} ({{ $category->blogs->count() }})</li></h5>
		@endforeach
	</ul>
</div>