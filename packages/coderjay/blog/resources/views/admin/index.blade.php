@extends('core::admin.templates.default')

@section('content')				
	<div class="col-sm-9">
	<legend><h2>Blogs</h2></legend>
		 <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
		      <div class="modal-content">

		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		          <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
		        </div>

		        <div class="modal-body">
		          <p>You are about to delete one track, this procedure is irreversible.</p>
		          <p>Do you want to proceed?</p>
		        </div>

		        <div class="modal-footer">
		          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		          <a class="btn btn-danger btn-ok">Delete</a>
		        </div>
		      </div>
		    </div>
		  </div>
		
		<a href="{{ route("admin.blog.create") }}">{{ Form::button('Add New Blog',['class' => "btn btn-primary"]) }}</a>
		<br><br>
		<table  class="table table-striped table-bordered table-hover table-condensed">
			<thead>
				<tr>
					<th>Id</th>
					<th>Title</th>
					<th>Category</th>
					<th>Published At</th>
					<th>Options</th>	
				</tr>
			</thead>
			<tbody>
				@foreach($blogs as $blog)
					<tr>
						<td>{{ $blog->id }}
						<td>{{ $blog->title }}</td>
						<td>{{ $blog->category['title'] }}</td>
						<td>{{ $blog->created_at->diffForHumans() }}</td>
						<td>
							<a href = "{{ route('admin.blog.edit',[$blog->id]) }} " >
								<button  type="button" class="btn btn-primary btn-xs">Edit</button>
							</a>
							<a href = "{{ route('blog.show',[$blog->id]) }}" target="_blank">
								<button  type="button" class="btn btn-primary btn-xs">View</button>
							</a>
							<button class="btn btn-primary btn-xs" data-href="{{ route('admin.blog.delete',[$blog->id]) }}" data-toggle=
							"modal" data-target="#confirm-delete">
						   		 Delete
						 	</button>

						 	@if($blog->status == 1)
						 		<a href = "{{ route('admin.blog.toggle_status',[$blog->id]) }}" >
									<button  type="button" class="btn btn-primary btn-xs">Enabled</button></a>
							@else
								<a href = "{{ route('admin.blog.toggle_status',[$blog->id]) }}" >
									<button  type="button" class="btn btn-danger btn-xs">Disabled</button></a>
							@endif
								
						</td>
					</tr>
				@endforeach
			</tbody>
			
		</table>
			{{ $blogs->appends(Request::except('page'))->links() }} 
	</div>
	@include('blog::admin.partials.categories')

  <script>
    $('#confirm-delete').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));

      $('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
    });
  </script>

@stop