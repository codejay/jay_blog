
@extends('core::admin.templates.default')

@section('content')
		<div class="col-sm-9">
			<h2>Add Blogs</h2><br>
			{!! Form::open(array('route' => array('admin.blog.store'), "files" => true)) !!}				

				<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('title', "Title") !!}
							{!! Form::text('title', "Blog Title" ,['class' => "form-control", 'placeholder' => "Blog Name"]) !!}
						</div>
					</div>

					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('slug', "Slug") !!}
							{!! Form::text('slug', "Blog slug" ,['class' => "form-control"]) !!}
						</div>
					</div>					

					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('category_id', "Blog Category") !!}
							{{-- {{ Form::select('category_id', $categories	, null ,['class' => "form-control"]) }} --}}
							{{ Form::select('category_id',  $all_categories	, null ,['class' => "form-control"]) }}

						</div>
					</div>

					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('tag_id', "Blog Tags") !!}
							{{ Form::select('tag_id', [0 => "test" ]	, null ,['class' => "form-control"]) }}
							{{-- {{ Form::select('category_id', $categories	, null ,['class' => "form-control"]) }} --}}
						</div>
					</div>

					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('status', "Blog Status") !!}
							{{ Form::select('status', ["1 "=> "Enabled","0" => "Disabled" ]	, null ,['class' => "form-control"]) }}
						</div>
					</div>

					<div class="col-sm-9">
						<div class="form-group">
							{!! Form::label('body', "Blog Description") !!}
							{!! Form::textarea('body', "Blog Body" ,[
								'class' => "form-control",
								'rows' => '5',
								'cols' => "8"
								]) !!}
						</div>
					</div>
					<p>
					<div class="col-sm-12">
						<fieldset>
							<legend>Image</legend>
							{{ Form::file('blog_image') }}
						</fieldset>
					</div><p><br><p>
					<div class="col-sm-10">
						<div class="form-group"><p><br>
							{!! Form::submit("Add Blog" ,['class' => "btn btn-primary"]) !!}
						</div>
					</div>
						
					{{ Form::hidden('user_id', \Auth::user()->id) }}	
				{!! Form::close() !!}			
		</div>

	@include('blog::admin.partials.categories')

		
			
@stop