@extends('core::admin.templates.default')

@section('content')
		<div class="col-sm-9">
			<h2>Edit Blogs</h2><br>
			 <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			    <div class="modal-dialog">
			      <div class="modal-content">

			        <div class="modal-header">
			          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			          <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
			        </div>

			        <div class="modal-body">
			          <p>You are about to delete Image, this procedure is irreversible.</p>
			          <p>Do you want to proceed?</p>
			        </div>

			        <div class="modal-footer">
			          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			          <a class="btn btn-danger btn-ok">Delete</a>
			        </div>
			      </div>
			    </div>
			  </div>

			{!! Form::open(array('route' => array('admin.blog.update',  $blog->id),'method' => 'put',"files" => true)) !!}
					
				
					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('title', "Title") !!}
							{!! Form::text('title',  $blog->title  ,['class' => "form-control"	]) !!}
						</div>
					</div>

					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('slug', "Slug") !!}
							{!! Form::text('slug',  $blog->slug  ,['class' => "form-control"]) !!}
						</div>
					</div>

					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('tag_id', "Blog Tags") !!}
							{{ Form::select('tag_id', [0 => "test" ]	, null ,['class' => "form-control"]) }}
							{{-- {{ Form::select('category_id', $categories	, null ,['class' => "form-control"]) }} --}}
						</div>
					</div>

						<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('status', "Blog Status") !!}
							{{ Form::select('status', ["1 "=> "Enabled","0" => "Disabled" ]	, null ,['class' => "form-control"]) }}
						</div>
					</div>

					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('category_id', "Blog Category") !!}
							{{-- {{ Form::select('category_id', [0 => $blog->category->name ]	, null ,['class' => "form-control"]) }} --}}
							{{ Form::select('category_id', [0 => "test" ]	, null ,['class' => "form-control"]) }}

						</div>
					</div>

					<div class="col-sm-9">
						<div class="form-group">
							{!! Form::label('body', "Blog Description") !!}
							{!! Form::textarea('body',  $blog->body  ,[
									'class' => "form-control",
									'rows' => "6",
							]) !!}
						</div>
					</div>
					<div class="col-sm-8">
						<div class="col-sm-12">
								<fieldset>
									<legend>Image</legend>
										@if($blog->media)	
											@foreach($blog->media as $media)
												@if(!file_exists(storage_path("app/media/".$media->id."/".$media->filename)))
													<div class="alert alert-warning">
													  <strong>Images Link is Broken!</strong> .
													</div>
													{{ Form::file('blog_image') }}				

												@else
		                          					  {{ Html::image('../storage/app/media/'.$media->id.'/'.$media->filename , 
					                         	   		$media->filename , [
					                            			"class" => "img-rounded",
					                            			"width" => 254,
					                            			"height" => 186
					                            		]) }}
					                            		<br>
					                            		<br>
														<a data-href = "{{ route("admin.blog.delete.media",["$blog->id"]) }}">
															<button class="btn btn-danger btn-xs"  data-toggle=
																"modal" data-target="#confirm-delete">
														   		 Delete
														 	</button>
														 </a>
		                        		 		 @endif 
											@endforeach
	
										@else						
											{{ Form::file('blog_image') }}								
										@endif
								</fieldset>
						</div>
							<p>
							<p><br>	
					</div>
					
					
					<div class="col-sm-9">
						<div class="form-group"><br>
							{!! Form::submit("Update Blog" ,['class' => "btn btn-primary"]) !!}
						</div>
					</div>	
					{{ Form::hidden('user_id', \Auth::user()->id) }}	
				{!! Form::close() !!}			
		</div>
	@include('blog::admin.partials.categories')
			
@stop